﻿using FizzBuzz.Business.Interfaces;
using FluentAssertions;
using Moq;
using System;
using Xunit;

namespace FizzBuzz.Business.Tests
{
    public class DayCheckTests
    {
        private Mock<IDayProvider> mockDayProvider;
        private DayCheck dayCheck;

        public DayCheckTests()
        {
            this.mockDayProvider = new Mock<IDayProvider>();
            this.dayCheck = new DayCheck(this.mockDayProvider.Object, DayOfWeek.Wednesday);

        }

        [Theory]
        [InlineData(DayOfWeek.Wednesday, true)]
        [InlineData(DayOfWeek.Monday, false)]
        [InlineData(DayOfWeek.Tuesday, false)]
        [InlineData(DayOfWeek.Thursday, false)]
        [InlineData(DayOfWeek.Friday, false)]
        [InlineData(DayOfWeek.Saturday, false)]
        [InlineData(DayOfWeek.Sunday, false)]

        public void IsDayEligibleForTextExchange(DayOfWeek getCurrentDay, bool ExpectedResult)
        {
            //Arrange
            this.mockDayProvider.Setup(x => x.GetCurrentDate()).Returns(getCurrentDay);

            //Act
            var result = this.dayCheck.GetDayOfWeek();

            //Assert
            result.Should().Be(ExpectedResult);
        }
    }
}
