﻿using FizzBuzz.Business.Interfaces;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace FizzBuzz.Business.Tests
{
    public class FizzBuzzManagerTests
    {
        private FizzBuzzManager fizzBuzzManager;
        private Mock<IDayCheck> dayCheck;
        private Mock<IDivisibilityChecks> divisibleByFive;
        private Mock<IDivisibilityChecks> divisibleByThree;
        private IList<IDivisibilityChecks> divisibilityChecks;

        public FizzBuzzManagerTests()
        {
            this.dayCheck = new Mock<IDayCheck>();
            this.divisibleByFive = new Mock<IDivisibilityChecks>();
            this.divisibleByThree = new Mock<IDivisibilityChecks>();
            this.divisibilityChecks = new List<IDivisibilityChecks>()
            {
                this.divisibleByThree.Object,this.divisibleByFive.Object
            };
            this.fizzBuzzManager = new FizzBuzzManager(this.divisibilityChecks);
        }

        [Fact]
        public void GetFizzBuzzListForGivenNumber()
        {
            List<string> numberList = new List<string>() { "1", "2", "3", "4", "5" };
            var res = this.fizzBuzzManager.GetFizzBuzzList(5).ToList();
            res.Should().BeEquivalentTo(numberList);

        }

        [Theory]
        [InlineData(15)]
        [InlineData(30)]
        public void GetFizzBuzzListForWeatherNumberDivisibleByThreeOrFive(int number)
        {
            //Arrange
            this.divisibleByThree.Setup(m => m.GetDivisibilityMessage(number)).Returns("FIZZ");
            this.divisibleByFive.Setup(m => m.GetDivisibilityMessage(number)).Returns("BUZZ");

            //Act
            var result = this.fizzBuzzManager.GetFizzBuzzList(number).ToList();

            //Assert
            result[number - 1].Should().BeEquivalentTo("FIZZ BUZZ");
        }


        public void GetFizzBuzzListForWeatherNumberDivisibleByThreeOrFiveOnFriday(int number)
        {
            //Arrange
            this.divisibleByThree.Setup(m => m.GetDivisibilityMessage(number)).Returns("WIZZ");
            this.divisibleByFive.Setup(m => m.GetDivisibilityMessage(number)).Returns("WUZZ");

            //Act
            var result = this.fizzBuzzManager.GetFizzBuzzList(number).ToList();

            //Assert
            result[number - 1].Should().BeEquivalentTo("WIZZ WUZZ");
        }
    }
}
