﻿using FizzBuzz.Business.Interfaces;
using FluentAssertions;
using Moq;
using Xunit;

namespace FizzBuzz.Business.Tests
{
    public class DivisibleByFiveTests
    {
        private DivisibleByFive divisibleByFive;
        private Mock<IDayCheck> dayCheck;

        public DivisibleByFiveTests()
        {
            this.dayCheck = new Mock<IDayCheck>();
            this.divisibleByFive = new DivisibleByFive(this.dayCheck.Object);

        }

        [Theory]
        [InlineData(false, 5, "BUZZ")]
        [InlineData(true, 10, "WUZZ")]
        [InlineData(true, 3, "")]

        public void IsDivisibleByFive(bool isMatched, int n, string expectedResult)
        {

            //Arrange
            this.dayCheck.Setup(x => x.GetDayOfWeek()).Returns(isMatched);

            //Act
            var output = this.divisibleByFive.GetDivisibilityMessage(n);

            //Assert
            output.Should().Be(expectedResult);
        }
    }
}
