﻿using FizzBuzz.Business.Interfaces;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.Business.Tests
{
    public class DivisibleByThreeTests
    {
        private DivisibleByThree divisibleByThree;
        private Mock<IDayCheck> dayCheck;

        public DivisibleByThreeTests()
        {
            this.dayCheck = new Mock<IDayCheck>();
            this.divisibleByThree = new DivisibleByThree(this.dayCheck.Object);
        }

        [Theory]
        [InlineData(false, 3, "FIZZ")]
        [InlineData(true, 6, "WIZZ")]
        [InlineData(true, 2, "")]
        public void IsDivisibleByThree(bool isMatched, int n, string expectedResult)
        {
            //Arrange
            this.dayCheck.Setup(x => x.GetDayOfWeek()).Returns(isMatched);

            //Act
            var output = this.divisibleByThree.GetDivisibilityMessage(n);

            //Assert
            output.Should().Be(expectedResult);
        }

    }
}
