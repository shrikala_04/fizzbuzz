﻿using FizzBuzz.Business.Constants;
using FizzBuzz.Business.Interfaces;

namespace FizzBuzz.Business
{
    public class DivisibleByFive : IDivisibilityChecks
    {
        private readonly IDayCheck dataCheck;

        public DivisibleByFive(IDayCheck dataCheck)
        {
            this.dataCheck = dataCheck;
        }

        public string GetDivisibilityMessage(int number)
        {
            return number % 5 == 0 ? this.dataCheck.GetDayOfWeek() ? Constant.WUZZ : Constant.BUZZ : string.Empty;
        }
    }
}
