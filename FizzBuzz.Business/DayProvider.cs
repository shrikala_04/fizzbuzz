﻿using FizzBuzz.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Business
{
    public class DayProvider:IDayProvider
    {
        public DayOfWeek GetCurrentDate()
        {
            return DateTime.UtcNow.DayOfWeek;
        }
    }
}
