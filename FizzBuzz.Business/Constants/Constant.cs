﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Business.Constants
{
   public class Constant
    {
        public const string FIZZ = "FIZZ";
        public const string BUZZ = "BUZZ";
        public const string WIZZ = "WIZZ";
        public const string WUZZ = "WUZZ";
    }
}
