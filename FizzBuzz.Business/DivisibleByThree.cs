﻿using FizzBuzz.Business.Constants;
using FizzBuzz.Business.Interfaces;

namespace FizzBuzz.Business
{
    public class DivisibleByThree : IDivisibilityChecks
    {
        private readonly IDayCheck dataCheck;

        public DivisibleByThree(IDayCheck dataCheck)
        {
            this.dataCheck = dataCheck;
        }

        public string GetDivisibilityMessage(int number)
        {
            return number % 3 == 0 ? this.dataCheck.GetDayOfWeek() ? Constant.WIZZ: Constant.FIZZ : string.Empty;
        }
    }
}
