﻿using FizzBuzz.Business.Interfaces;
using System;

namespace FizzBuzz.Business
{
    public class DayCheck : IDayCheck
    {
        private DayOfWeek dayOfWeek;
        private IDayProvider dayProvider;

        public DayCheck(IDayProvider dayProvider, DayOfWeek dayOfWeek)
        {
            this.dayProvider = dayProvider;
            this.dayOfWeek = dayOfWeek;
        }

        public bool GetDayOfWeek()
        {
            return this.dayProvider.GetCurrentDate() == this.dayOfWeek;
        }
    }
}
