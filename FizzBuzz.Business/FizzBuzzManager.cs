﻿using FizzBuzz.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzz.Business
{
    public class FizzBuzzManager : IFizzBuzzManager
    {
        private readonly IEnumerable<IDivisibilityChecks> divisibilityCheckList;

        public FizzBuzzManager(IEnumerable<IDivisibilityChecks> divisibilityCheckList)
        {
            this.divisibilityCheckList = divisibilityCheckList;
        }

        public IEnumerable<string> GetFizzBuzzList(int number)
        {
            var finalList = new List<string>();
            for (int i = 1; i <= number; i++)
            {
                var matchedCaseChecks = string.Join(" ", this.divisibilityCheckList.Select(x => x.GetDivisibilityMessage(i)));
                finalList.Add(string.IsNullOrWhiteSpace(matchedCaseChecks) ? i.ToString() : matchedCaseChecks);
            }

            return finalList;
        }
    }
}
