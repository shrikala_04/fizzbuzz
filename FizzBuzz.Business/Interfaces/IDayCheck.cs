﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Business.Interfaces
{
   public interface IDayCheck
    {
        bool GetDayOfWeek();
    }
}
