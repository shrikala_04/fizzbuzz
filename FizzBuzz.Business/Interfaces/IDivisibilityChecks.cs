﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Business.Interfaces
{
   public interface IDivisibilityChecks
    {
        string GetDivisibilityMessage(int number);
    }
}
