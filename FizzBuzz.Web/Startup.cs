using FizzBuzz.Business;
using FizzBuzz.Business.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public  void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddScoped<IDivisibilityChecks, DivisibleByThree>();
            services.AddScoped<IDivisibilityChecks, DivisibleByFive>();
            services.AddScoped<IFizzBuzzManager, FizzBuzzManager>();
            services.AddScoped<IDayProvider, DayProvider>();
            services.AddScoped<IDayCheck>(x => new DayCheck(x.GetRequiredService<IDayProvider>(),DayOfWeek.Wednesday));
            // services.AddScoped<IDayCheck, DayCheck>().AddSingleton<DayOfWeek>("dayOfWeek").Is(DayOfWeek.Friday);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=FizzBuzz}/{action=Index}/{id?}");
            });
        }
    }
}
