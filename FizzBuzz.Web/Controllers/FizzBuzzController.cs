﻿using FizzBuzz.Business.Interfaces;
using FizzBuzz.Web.Models;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace FizzBuzz.Web.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzManager FizzBuzz;
        public FizzBuzzController(IFizzBuzzManager FizzBuzz)
        {
            this.FizzBuzz = FizzBuzz;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpGet]
        public ActionResult GetNextPage(FizzBuzzModel ticTacModel, int? page)
        {
            var resultList = this.FizzBuzz.GetFizzBuzzList(ticTacModel.Number);
            ticTacModel.Number = ticTacModel.Number;
            ticTacModel.Result = resultList.ToPagedList(page ?? 1, 20);
            return View("Index", ticTacModel);
        }

        [HttpPost]
        public ActionResult Index(FizzBuzzModel model, int? page)
        {
            if(!ModelState.IsValid)
            {
                return View("Index");
            }
            model.Result = this.FizzBuzz.GetFizzBuzzList(model.Number).ToPagedList(page ?? 1, 20);
            return this.View("Index",model);
        }
    }
}
