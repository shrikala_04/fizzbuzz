using System;
using System.ComponentModel.DataAnnotations;
using X.PagedList;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "Please enter the number")]
        [Range(1, 1000, ErrorMessage = "Enter Number between 1 to 1000")]
        [Display(Description = "Please enter the number")]

        public int Number { get; set; }

        public IPagedList<String> Result { get; set; }
    }
}
