﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Web.Helper
{
    public static class HtmlExtentions
    {
        public static IHtmlContent RenderTicTacList(this IHtmlHelper html, string item)
        {
            var output = new StringBuilder();
            var isColorEnabled = item.Contains(" ");
            var arrayofitems = item.Split(' ').Where(x => !string.IsNullOrWhiteSpace(x));
            foreach (var arrayofitem in arrayofitems)
            {
                var attribute = isColorEnabled ? $"class ='{arrayofitem}'" : string.Empty;
                output.Append($"<span {attribute}>{arrayofitem}</span>");
            }

            return new HtmlString(output.ToString());
        }
    }
}
