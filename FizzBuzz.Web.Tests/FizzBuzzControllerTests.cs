using FizzBuzz.Business.Interfaces;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace FizzBuzz.Web.Tests
{
    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzManager> fizzBuzzManager;
        private FizzBuzzController fizzBuzzController;
        private FizzBuzzModel fizzBuzzModel;
        private List<string> fizzBuzzList;

        public FizzBuzzControllerTests()
        {
            this.fizzBuzzManager = new Mock<IFizzBuzzManager>();
            this.fizzBuzzController = new FizzBuzzController(this.fizzBuzzManager.Object);
            this.fizzBuzzList = new List<string>() { "1", "2", "FIZZ", "4", "BUZZ" };
        }

        [Fact]
        public void FizzBuzzController_Should_Return_Index_View()
        {
            //Act
            var result = this.fizzBuzzController.Index() as ViewResult;

            //Assert
            result.ViewName.Should().Be("Index");
        }

        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(15)]
        public void FizzBuzzController_Should_Return_FizzBuzzList_For_ValidInput(int number)
        {
            //Arrange
            int page = 1;
            this.fizzBuzzModel = new FizzBuzzModel() { Number = number };
            this.fizzBuzzManager.Setup(x => x.GetFizzBuzzList(number)).Returns(this.fizzBuzzList);

            //Act
            var result = this.fizzBuzzController.Index(this.fizzBuzzModel, page) as ViewResult;

            //Assert
            result.Model.Should().BeEquivalentTo(this.fizzBuzzModel);
            result.ViewName.Should().Be("Index");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(100000)]
        public void FizzBuzzModel_Should_Return_VallidationError_For_InValidInput(int number)
        {
            //Arrange
            int page = 1;
            this.fizzBuzzModel = new FizzBuzzModel() { Number = number };
            this.fizzBuzzController.ModelState.AddModelError("Number", "please enter a number between 0 to 1000");

            //Act
            var viewresult = this.fizzBuzzController.GetNextPage(this.fizzBuzzModel, page) as ViewResult;

            //Assert
            viewresult.Should().NotBeNull();
            viewresult.Model.Should().BeEquivalentTo(this.fizzBuzzModel);
            viewresult.ViewData.ModelState.IsValid.Should().BeFalse();
        }
    }
}
